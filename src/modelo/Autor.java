package modelo;

public class Autor {
	
	public int id;
	public String nome;
	
	
	public Autor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Autor(int id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
