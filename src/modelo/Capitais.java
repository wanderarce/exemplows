package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="capitais")
public class Capitais {

	private List<Metar> metar= new ArrayList<Metar>();

	public List<Metar> getMetar() {
		return metar;
	}

	public void setMetar(List<Metar> metar) {
		this.metar = metar;
	}
	
	
	
}
