package controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import modelo.Capitais;

/**
 * Servlet implementation class condicoes
 */
@WebServlet("/condicoes")
public class condicoes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public condicoes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			listaCapitais(request, response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	public void listaCapitais(HttpServletRequest request, HttpServletResponse response ) throws JAXBException,ServletException,IOException{
		
		try {

			URL url= new URL("http://servicos.cptec.inpe.br/XML/capitais/condicoesAtuais.xml");
			
			JAXBContext context=JAXBContext.newInstance(Capitais.class);
			
			Unmarshaller u= context.createUnmarshaller();
			
			Capitais capitais= (Capitais) u.unmarshal(url);
			
			RequestDispatcher r= request.getRequestDispatcher("condicoes.jsp");
			request.setAttribute("condicoes", capitais);
			r.forward(request, response);
			
		} catch(MalformedURLException e){
			e.printStackTrace();
		}catch (ServletException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}catch (JAXBException e) {
			e.printStackTrace();
		}
		
	}
}
