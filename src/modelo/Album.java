package modelo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
public class Album {
	
	public int id;
	public String nome;
	
	private Autor autor;
	
	private List<Musica> musicas;

	public Album() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Album(int id, String nome, Autor autor, List<Musica> musicas) {
		super();
		this.id = id;
		this.nome = nome;
		this.autor = autor;
		this.musicas = musicas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public List<Musica> getMusicas() {
		return musicas;
	}

	public void setMusicas(List<Musica> musicas) {
		this.musicas = musicas;
	}
	
	
	

}
