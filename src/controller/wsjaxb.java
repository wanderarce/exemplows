package controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import modelo.Cidades;


/**
 * Servlet implementation class wsjaxb
 */
@WebServlet("/wsjaxb")
public class wsjaxb extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public wsjaxb() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			listaCidadesJax(request, response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	private void listaCidadesJax(HttpServletRequest request,
			HttpServletResponse response) throws JAXBException, ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			URL url = new URL("http://servicos.cptec.inpe.br/XML/listaCidades");
			JAXBContext context = JAXBContext.newInstance(Cidades.class);
			// tipo de objeto usado para transformar um xml para objet
			Unmarshaller um = context.createUnmarshaller();
			// transforma xml em object		
			Cidades cidades = (Cidades) um.unmarshal(url);			
			
			RequestDispatcher rd = request.getRequestDispatcher("cidades_jax.jsp");
			request.setAttribute("cidades_jax", cidades);
			rd.forward(request, response);
						
		}catch(MalformedURLException e){
			e.printStackTrace();
		}catch (ServletException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
