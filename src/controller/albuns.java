package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Album;
import modelo.Autor;
import modelo.Musica;

/**
 * Servlet implementation class albuns
 */
@WebServlet("/albuns")
public class albuns extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public albuns() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private List<Musica> getMusicas(){

		List<Musica> musicas= new ArrayList<Musica>();

		Musica musica= new Musica();
		musica.setId(1);
		musica.setNome("onibus");
		musica.setDuracao(3.00);

		Musica musica2= new Musica();
		musica.setId(2);
		musica.setNome("Vamos invadir sua praia");
		musica.setDuracao(3.50);

		musicas.add(musica);
		musicas.add(musica2);

		return musicas;

	}

	private Album getAlbum(){

		Autor autor= new Autor();
		autor.setId(1);
		autor.setNome("Ultraje a Rigor");

		Album album= new Album();
		album.setId(1);
		album.setNome("Acustico");
		album.setAutor(autor);
		album.setMusicas(getMusicas());

		return album;
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Album al= getAlbum();

		String xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xml+="<album>";
		xml+=	"<autor>";
		xml+=		"<id>"+ al.getAutor().getId() + "</id>";
		xml+=		"<nome>"+ al.getAutor().getNome() + "</nome>";
		xml+=	"</autor>";
		xml+=	"<id>"+ al.getId() + "</id>";
		xml+=	"<musicas>";
		for(Musica m: al.getMusicas()){
			xml+=		"<musica>";
			xml+=			"<id>"+ m.getId() + "</id>";
			xml+=			"<nome>"+ m.getNome() + "</nome>";
			xml+=			"<duracao>"+ m.getDuracao() + "</duracao>";
			xml+= 		"</musica>";
		}
		xml+=	"</musicas>";
		xml+=	"<nome>"+al.getNome() + "</nome>";
		xml+="</album>";
		
		response.setContentType("text/xml");
		PrintWriter out= response.getWriter();
		out.print(xml);
		out.close();
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}*/

}
