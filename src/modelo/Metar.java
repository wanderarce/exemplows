package modelo;

public class Metar {
	
	private String codigo;
	private String atualizacao;
	private int pressao;
	private double temperatura;
	private String tempo;
	private String tempo_desc;
	private int umidade;
	private int vento_dir;
	private int vento_int;
	private int intensidade;
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getAtualizacao() {
		return atualizacao;
	}
	public void setAtualizacao(String atualizacao) {
		this.atualizacao = atualizacao;
	}
	public int getPressao() {
		return pressao;
	}
	public void setPressao(int pressao) {
		this.pressao = pressao;
	}
	public double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}
	public String getTempo() {
		return tempo;
	}
	public void setTempo(String tempo) {
		this.tempo = tempo;
	}
	public String getTempo_desc() {
		return tempo_desc;
	}
	public void setTempo_desc(String tempo_desc) {
		this.tempo_desc = tempo_desc;
	}
	public int getUmidade() {
		return umidade;
	}
	public void setUmidade(int umidade) {
		this.umidade = umidade;
	}
	public int getVento_dir() {
		return vento_dir;
	}
	public void setVento_dir(int vento_dir) {
		this.vento_dir = vento_dir;
	}
	public int getVento_int() {
		return vento_int;
	}
	public void setVento_int(int vento_int) {
		this.vento_int = vento_int;
	}
	public int getIntensidade() {
		return intensidade;
	}
	public void setIntensidade(int intensidade) {
		this.intensidade = intensidade;
	}
	

}
