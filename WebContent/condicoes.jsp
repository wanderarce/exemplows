<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>

<%@page import="modelo.*"%>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>WS-cidades</title>
<%
List<Metar> metar=null;
if(request.getAttribute("condicoes")!=null){
	metar=( (Capitais) request.getAttribute("condicoes")).getMetar();
}
%>
</head>
<body>

<%	if(metar!=null){%>
		<table border="1">
			<tr>
				<th>Código</th>
				<th>Atualização</th>
				<th>Pressão</th>
				<th>Temperatura</th>
				<th>Tempo</th>
				<th>Tempo Descrição</th>
				<th>Umidade</th>
				<th>Direção</th>
				<th>Intensidade</th>
			</tr>
			<% for(Metar m: metar){ %>
			<tr>
				<td><%=m.getCodigo() %></td>
				<td><%=m.getAtualizacao() %></td>
				<td><%=m.getPressao() %></td>
				<td><%=m.getTemperatura() %></td>
				<td><%=m.getTempo()%></td>
				<td><%=m.getTempo_desc() %></td>
				<td><%=m.getUmidade() %></td>
				<td><%=m.getVento_dir() %></td>
				<td><%=m.getVento_int() %></td>
			<% } %>
			</tr>
		</table>
		
	<% } %>
</body>
</html>